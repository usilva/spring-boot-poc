package fr.bucares.SpringBootPOC.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.bucares.SpringBootPOC.model.Person;
import fr.bucares.SpringBootPOC.repository.PersonRepository;

@Service
public class PersonService {
	
	@Autowired
	private PersonRepository personRepository;
	
	public List<Person> getAllPerson(){
		return personRepository.findAll();
	}
	
	public Person getPersonById(Long id){
		ArrayList<Long> idList = new ArrayList<>();
		idList.add(id);
		List<Person> persons = null;
		
		return (persons = personRepository.findAllById(idList)).isEmpty()
				? null : persons.get(0);
	}
	
	public Person savePerson(Person person){
		return personRepository.save(person);
	}
	
	public void removePerson(Person person){
		personRepository.delete(person);
	}

}
