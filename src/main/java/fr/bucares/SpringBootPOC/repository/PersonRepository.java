package fr.bucares.SpringBootPOC.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.bucares.SpringBootPOC.model.Person;

public interface PersonRepository extends JpaRepository<Person, Long> {

}
