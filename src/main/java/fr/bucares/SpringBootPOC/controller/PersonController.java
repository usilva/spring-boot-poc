package fr.bucares.SpringBootPOC.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.bucares.SpringBootPOC.model.Person;
import fr.bucares.SpringBootPOC.service.PersonService;

@RestController
public class PersonController {
	
	@Autowired
	private PersonService personService;
	
	@Value("${build.version}")
    private String buildVersion;
	
	@RequestMapping(value = "/version", method=RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Map<String, String>> getVersion() {
		Map<String, String> response = new HashMap<>();
		response.put("version", buildVersion);
		
		return new ResponseEntity<Map<String, String>>(response, HttpStatus.OK);
    }
	
	@RequestMapping(value="/person", method=RequestMethod.GET)
	public ResponseEntity<List<Person>> getAllPerson(){
		return new ResponseEntity<List<Person>>(personService.getAllPerson(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/person/{id}", method = RequestMethod.GET)
	public ResponseEntity<Person> getPersonById(@PathVariable("id") long id) {
		Person person = personService.getPersonById(id);
			
		if (person == null) {
			return new ResponseEntity<Person>(new Person("", ""), HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Person>(person, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/person", method = RequestMethod.POST, produces = "application/json")
   	public ResponseEntity<Person> savePerson(@RequestParam("name") String name,
   											@RequestParam("lastName") String lastName) {
		Person person = new Person(name, lastName);
		
		return new ResponseEntity<Person>(personService.savePerson(person), HttpStatus.CREATED);
   	}
	
	@RequestMapping(value = "/person/{id}", method = RequestMethod.PUT)
   	public ResponseEntity<Person>  updatePerson(@PathVariable("id") long id,
   											   @RequestParam("name") String name,
											   @RequestParam("lastName") String lastName) {
    	Person person = new Person(name, lastName);
    	person.setId(id);
    	
		return new ResponseEntity<Person>(personService.savePerson(person),
										   HttpStatus.OK);
   	}
	
	@RequestMapping(value = "/person/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> removePersonById(@PathVariable("id") long id){
    	Person person = personService.getPersonById(id);
    	
    	if (person == null) {
			return new ResponseEntity<String>("Person does not exist", HttpStatus.NOT_FOUND);
		}
    	
    	personService.removePerson(person);
		return new ResponseEntity<String>("Person was removed", HttpStatus.OK);
	}

}
