package fr.bucares.SpringBootPOC.security;


import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        	.csrf().disable()
        	.authorizeRequests()
        		.antMatchers("/person", "/person/**", "/version").hasRole("USER")
        		.anyRequest().authenticated()
        	.and()
        	.httpBasic()
            .and()
            .cors()
            .and()
            .formLogin()
                .permitAll()
                .and()
            .logout()
            	.permitAll();
    }

       
}


