package fr.bucares.SpringBootPOC.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import fr.bucares.SpringBootPOC.repository.PersonRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PersonTest {

	@Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private PersonRepository personRepository;
    
    private Person testPerson = new Person();
    
	@Before
	public void setUp() throws Exception {
		testPerson.setName("Jhon");
	    testPerson.setLastName("Doe");
	    testPerson = entityManager.persistAndFlush(testPerson);
	}

	@Test
	public void test() {
		
	    Person person = null;
	    ArrayList<Long> idList = new ArrayList<>();
		idList.add(testPerson.getId());
		List<Person> persons = personRepository.findAllById(idList);
	 
		assertFalse(persons.isEmpty());
		person = persons.get(0);
		assertEquals(testPerson.getId(), person.getId());
		assertEquals(testPerson.getName(), person.getName());
		assertEquals(testPerson.getLastName(), person.getLastName());
	}

}
