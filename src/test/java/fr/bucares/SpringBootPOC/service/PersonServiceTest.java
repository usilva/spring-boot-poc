package fr.bucares.SpringBootPOC.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import fr.bucares.SpringBootPOC.model.Person;
import fr.bucares.SpringBootPOC.repository.PersonRepository;

@RunWith(SpringRunner.class)
public class PersonServiceTest {

	@TestConfiguration
    static class PersonServiceTestContextConfiguration {
  
        @Bean
        public PersonService personService() {
            return new PersonService();
        }
    }
	
	@Autowired
    private PersonService personService;
 
    @MockBean
    private PersonRepository personRepository;
    
    private Person testPerson = new Person("John", "Doe");
    
	@Before
	public void setUp() throws Exception {
		testPerson.setId(1);
		ArrayList<Long> idList = new ArrayList<>();
		idList.add(testPerson.getId());
		List<Person> persons = new ArrayList<>();
		persons.add(testPerson);
		
		Mockito.when(personRepository.save(testPerson))
	      .thenReturn(testPerson);
		
	    Mockito.when(personRepository.findAll())
	      .thenReturn(persons);
	    
	    Mockito.when(personRepository.findAllById(idList))
	      .thenReturn(persons);
	    	    
	    Mockito.doNothing().when(personRepository).delete(testPerson);
	}
	
	@Test
	public void testSavePerson() {
	    Person person = personService.savePerson(testPerson);
	  
		assertEquals(testPerson.getId(), person.getId());
		assertEquals(testPerson.getName(), person.getName());
		assertEquals(testPerson.getLastName(), person.getLastName());
	}
	
	@Test
	public void testGetAllPerson() {
	    List<Person> persons = personService.getAllPerson();
	    
	    assertEquals(1, persons.size());	  
		assertEquals(testPerson.getId(), persons.get(0).getId());
		assertEquals(testPerson.getName(), persons.get(0).getName());
		assertEquals(testPerson.getLastName(), persons.get(0).getLastName());
	}

	@Test
	public void testGetPersonByID() {
	    Person person = personService.getPersonById(testPerson.getId());
	  
	    assertNotNull(person);
		assertEquals(testPerson.getId(), person.getId());
		assertEquals(testPerson.getName(), person.getName());
		assertEquals(testPerson.getLastName(), person.getLastName());
	}
	
	@Test
	public void testRemovePerson() {
	    personService.removePerson(testPerson);
	    
	    Mockito.verify(personRepository, times(1)).delete(testPerson);
	    
	}
	
	@Test
	public void testGetPersonByIDNotFound() {
		Person person = personService.getPersonById(100L);
	    
	    assertNull(person);
	    
	}
}
