package fr.bucares.SpringBootPOC.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.times;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import fr.bucares.SpringBootPOC.model.Person;
import fr.bucares.SpringBootPOC.service.PersonService;

@RunWith(SpringRunner.class)
@WebMvcTest(PersonController.class)
public class PersonControllerTest {

	@Autowired
    private MockMvc mvc;
 
    @MockBean
    private PersonService service;
    
    @Value("${build.version}")
    private String buildVersion;
    
    private Person person = new Person("John", "Doe");
	
	@Before
	public void setUp() throws Exception {
		person.setId(1L);
	}

	@Test
	public void testGetVersion() throws Exception {		
		mvc.perform(get("/version")
					.accept(MediaType.APPLICATION_JSON)
					.with(httpBasic("bucares", "buc4r3s")))
		   .andExpect(status().isOk())
		   .andExpect(jsonPath("$.version", is(buildVersion)));
	}
	
	@Test
	public void testSavePerson() throws Exception {
		Person jane = new Person("Jane", "Doe");
		
		Mockito.when(service.savePerson(jane))
	      .thenReturn(jane);
		
		mvc.perform(post("/person")
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.with(httpBasic("bucares", "buc4r3s"))
					.with(csrf())
					.param("name", jane.getName())
					.param("lastName", jane.getLastName()))
		   .andExpect(status().isCreated());
	}
	
	@Test
	public void testGetAllPersons() throws Exception {
		List<Person> persons = new ArrayList<Person>();
		persons.add(person);
		Mockito.when(service.getAllPerson())
	      .thenReturn(persons);
		
		mvc.perform(get("/person")
					.accept(MediaType.APPLICATION_JSON)
					.with(httpBasic("bucares", "buc4r3s")))
		   .andExpect(status().isOk())
		   .andExpect(jsonPath("$", hasSize(1)))
		   .andExpect(jsonPath("$[0].name", is(person.getName())))
		   .andExpect(jsonPath("$[0].lastName", is(person.getLastName())));
	}
	
	@Test
	public void testGetPersonById() throws Exception {
		Mockito.when(service.getPersonById(1L))
	      .thenReturn(person);
		
		mvc.perform(get("/person/1")
					.accept(MediaType.APPLICATION_JSON)
					.with(httpBasic("bucares", "buc4r3s")))
		   .andExpect(status().isOk())
		   .andExpect(jsonPath("$.id", is(1)))
		   .andExpect(jsonPath("$.name", is(person.getName())))
		   .andExpect(jsonPath("$.lastName", is(person.getLastName())));
	}
	
	@Test
	public void testGetPersonByIdNotFound() throws Exception {
		Person p = null;
		Mockito.when(service.getPersonById(1L))
	      .thenReturn(p);
		
		mvc.perform(get("/person/1000")
					.accept(MediaType.APPLICATION_JSON)
					.with(httpBasic("bucares", "buc4r3s")))
		   .andExpect(status().isNotFound())
		   .andExpect(jsonPath("$.name", is("")))
		   .andExpect(jsonPath("$.lastName", is("")));
	}
	
	@Test
	public void testUpdatePerson() throws Exception {
		person.setName("Jane");
		
		Mockito.when(service.savePerson(person))
	      .thenReturn(person);
		
		mvc.perform(put("/person/1")
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.with(httpBasic("bucares", "buc4r3s"))
					.with(csrf())
					.param("name", person.getName())
					.param("lastName", person.getLastName()))
		   .andExpect(status().isOk());

	}
	
	@Test
	public void testRemovePerson() throws Exception {
		Mockito.when(service.getPersonById(1L))
	      .thenReturn(person);
		
		mvc.perform(delete("/person/1")
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.with(httpBasic("bucares", "buc4r3s"))
					.with(csrf()))
		   .andExpect(status().isOk());
		
		Mockito.verify(service, times(1)).removePerson(person);
	}
	
	@Test
	public void testRemovePersonNotFound() throws Exception {
		mvc.perform(delete("/person/1000")
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.with(httpBasic("bucares", "buc4r3s"))
					.with(csrf()))
		   .andExpect(status().isNotFound());
	}
}
