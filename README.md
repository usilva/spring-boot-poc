# Spring Boot POC Project

This is a POC Spring  (**backend service**) based on Spring Boot and Maven, integrated with Spring Security and Spring Data H2

## Requirements

For building and running the application you need:

- [JDK 1.8]
- [Maven 3]

## Running the application

To  get the executable jar package of the Spring Boot application, you need to do:

```shell
mvn clean package
```

Then, to execute the application, you need to:


```shell
java -Dserver.port={PORT} -jar target/SpringBootPOC-{VERSION_PROJECT}.jar
```
#### Note: 
- {PORT} define the port to be used, if this parameter is not set, by default it is defined as **8080**
- {VERSION_PROJECT} define the project version

## Running Test Unit

To run the unit tests and generate the code coverage report, you need to:

```shell
mvn clean test
```

```shell
mvn jacoco:report
```

This will create by default the following folder:

* target/site/jacoco/

This is the route that contains all the details of the coverage report. All these reports are automatically appended to the file ** index.html **.


## Autentication Spring Segurity

This integrates Spring Security API. Use the following authenticated user credentials to access the REST services. Logging in with 

- User : **bucares**
- Password : **buc4r3s**

Spring Security **CSRF** protection is enabled, so you need to make sure you use the token for anything that modifies state (POST, PUT, and DELETE).

## API Services

 **GET service that returns the version of the project :**

- http://127.0.0.1/:{PORT}/version

 **POST service that creates a person :**

- http://127.0.0.1/:{PORT}/person

 **UPATE service that updates a person**

- http://127.0.0.1/:{PORT}/person/{id}

 **DELETE service that removes a person**

- http://127.0.0.1/:{PORT}/person/{id}

#### Note: 
- {id} defines the unique identifier of each person
- Person parameters: {"id" : long, "name": String, "lastName": String}
